# Cookieapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing

1. Fill the form by passing user name and click on submit button.
2. Check the developers console logs for lifecyle of cookie.
