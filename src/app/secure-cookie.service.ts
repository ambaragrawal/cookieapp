/// <reference types="crypto-js" />

import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class SecureCookieService {

  constructor() { }

  key: string = "randomkeytosecurecookie";

  encryptCookie(originalValue): string {
    return CryptoJS.AES.encrypt(originalValue, this.key).toString();
  }

  decryptCookie(encryptedValue): string {
    let bytes  = CryptoJS.AES.decrypt(encryptedValue, this.key);
    return bytes.toString(CryptoJS.enc.Utf8);
  }
}
