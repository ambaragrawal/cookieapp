import { TestBed, inject } from '@angular/core/testing';

import { SecureCookieService } from './secure-cookie.service';

describe('SecureCookieService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecureCookieService]
    });
  });

  it('should be created', inject([SecureCookieService], (service: SecureCookieService) => {
    expect(service).toBeTruthy();
  }));
});
