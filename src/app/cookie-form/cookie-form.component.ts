import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { SecureCookieService } from '../secure-cookie.service';

@Component({
  selector: 'app-cookie-form',
  templateUrl: './cookie-form.component.html',
  styleUrls: ['./cookie-form.component.css']
})
export class CookieFormComponent implements OnInit {

  constructor(
    private cookieService: CookieService,
    private secureCookieService: SecureCookieService
  ) { }

  cookie = {
    secretToken: 'somerandomsecrettoken'
  };

  cookieValue: string = '';

  cookieParameter = {
    name: 'secretToken',
    value: '',
    expires: new Date(new Date().getTime()+60*60*1000*24),
    path: undefined,
    domain: undefined,
    secure: false
  }

  Register(cookieForm: NgForm) {  
    console.log('Form Values: ', cookieForm.value);

    //Encrypt cookie value
    console.log('Origional cookie value that is going to set: ', cookieForm.value.secretToken);
    this.cookieParameter.value = this.secureCookieService.encryptCookie(this.cookie.secretToken);
    console.log('Encrypted cookie value that is going to set: ', this.cookieParameter.value);

    //Set cookie
    console.log('Setting cookie.....');
    this.cookieService.set(
      this.cookieParameter.name,
      this.cookieParameter.value,
      this.cookieParameter.expires,
      this.cookieParameter.path,
      this.cookieParameter.domain,
      this.cookieParameter.secure
    );
    console.log('Cookie is set!');

    //Get cookie
    let cookieValue = this.cookieService.get('secretToken');

    //Decrypt cookie value
    console.log('Encrypted cookie value from browser: ', cookieValue);
    this.cookieValue = this.secureCookieService.decryptCookie(cookieValue);
    console.log('Orional cookie value from browser: ', this.cookie.secretToken);
    if (this.cookieValue === this.cookie.secretToken) {
      console.log('Cookie value is not tempered!')
      this.cookie.secretToken = this.cookieValue;
    } else {
      console.log('Cookie value is tempered!');
    }
  }

  ngOnInit(): void {
    
  } 

}
