import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { CookieFormComponent } from './cookie-form/cookie-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CookieFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
